import Tweakpane from '../web_modules/tweakpane.js';
import copy from '../web_modules/copy-text-to-clipboard.js';

import { Visualiser } from './vis.js';

const vis = new Visualiser('map', {
  restore: true,
});

window.vis = vis;

const elInput = document.getElementById('code');
elInput.value = vis.getCode();
elInput.addEventListener('input', (e) => {
  vis.setCode(e.target.value);
});

/**
 * TweakPane
 */
const pane = new Tweakpane({
  title: 'Settings',
});
const btnToggle = pane.addButton({
  title: 'Start/stop',
});

btnToggle.on('click', () => {
  vis.toggle();
});
const btnClipBoard = pane.addButton({
  title: 'Save and copy to clipboard',
});

btnClipBoard.on('click', () => {
  const url = vis.urlSave();
  if (url) {
    copy(url.toString());
  }
});

const stateGrid = vis.config.state.grid;
const stateMain = vis.config.state.main;
const stateMap = vis.config.state.map;

const grid = pane.addFolder({ title: 'Animation' });

grid.addInput(stateGrid, 'colorA');
grid.addInput(stateGrid, 'colorB');
grid.addInput(stateMap, 'lightColor').on('change', vis.updateLightColor);
grid
  .addInput(stateMap, 'backgroundColor')
  .on('change', vis.updateBackgroundColor);

grid
  .addInput(stateGrid, 'nX', {
    step: 1,
    min: 1,
    max: 100,
  })
  .on('change', vis.updateGrid);

grid
  .addInput(stateGrid, 'nY', {
    step: 1,
    min: 1,
    max: 100,
  })
  .on('change', vis.updateGrid);
grid
  .addInput(stateGrid, 'nVertices', {
    step: 1,
    min: 3,
    max: 10,
  })
  .on('change', vis.updateGrid);
grid
  .addInput(stateGrid, 'radiusDeg', {
    step: 0.01,
    min: 0.1,
    max: 2,
  })
  .on('change', vis.updateGrid);
grid
  .addInput(stateGrid, 'orientationDeg', {
    step: 1,
    min: 0,
    max: 360,
  })
  .on('change', vis.updateGrid);

grid.addInput(stateMain, 'speedFactor', {
  step: 0.1,
  min: 1,
  max: 100,
});

grid.addInput(stateGrid, 'heightFactor', {
  step: 100,
  min: 0,
  max: 1e6,
});
grid.addInput(stateGrid, 'heightMax', {
  step: 100,
  min: 0,
  max: 1e6,
});

/*grid.addInput(stateMap, 'lightDistance',{*/
   //step:0.1,
    //min :0,
  //max : 50
//}).on('change', vis.updateLightPosition);

//grid
  //.addInput(stateMap, 'lightAzimutal', {
    //step: 1,
    //min: 0,
    //max: 360,
  //})
  //.on('change', vis.updateLightPosition);
//grid
  //.addInput(stateMap, 'lightPolar', {
    //step: 1,
    //min: 0, // midday
    //max: 360
  //})
  /*.on('change', vis.updateLightPosition);*/
