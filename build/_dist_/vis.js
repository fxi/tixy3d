import mapboxgl from '../web_modules/mapbox-gl.js';
import '../web_modules/mapbox-gl/dist/mapbox-gl.css.proxy.js';
import { onNextFrame } from './animation_frame.js';

const defaults = {
  width: null,
  height: null,
  restore: false,
  code_default: 'cos(hypot(x-=25,y-=25)/2-t+atan2(x,y))',
  state: {
    enabled: {},
    map: {
      lightColor: '#f8f2f2',
      lightDistance: 1.15,
      lightAzimutal: 210,
      lightPolar: 30,
      backgroundColor: '#363636',
      pitch: 60,
      bearing: -92.00000000000006,
      zoom: 3.8082654565035883,
      center: {
        lng: 2.4991753332308235,
        lat: -1.8068441227441667,
      },
    },
    grid: {
      colorA: 'rgb(207, 207, 207)',
      colorB: 'rgb(51, 51, 51)',
      radiusDeg: 0.5,
      orientationDeg: 0,
      nX: 50,
      nY: 50,
      heightFactor: 423900,
      heightMax: 1000000,
      nVertices: 4,
    },
    main: {
      speedFactor: 23.6,
      code: 'cos(hypot(x-=25,y-=25)/2-t+atan2(x,y))',
      offset: {
        x: 0,
        y: 0,
      },
    },
  },
};

class Visualiser {
  constructor(id, config) {
    const vis = this;
    vis.id = id;
    vis.config = Object.assign({}, defaults, config);
    if (config.restore) {
      vis.urlRestore();
    }
    vis.init();
    return vis;
  }

  init() {
    const vis = this;
    /**
     * binds. mmh ?
     */
    vis.toggle = vis.toggle.bind(vis);
    vis.start = vis.start.bind(vis);
    vis.stop = vis.stop.bind(vis);
    vis.render = vis.render.bind(vis);
    vis.urlSave = vis.urlSave.bind(vis);
    vis.urlRestore = vis.urlRestore.bind(vis);
    vis.updateGrid = vis.updateGrid.bind(vis);
    vis.updateBackgroundColor = vis.updateBackGroundColor.bind(vis);
    vis.updateLightColor = vis.updateLightColor.bind(vis);
    vis.updateLightPosition = vis.updateLightPosition.bind(vis);
    vis.saveMapPos = vis.saveMapPos.bind(vis);

    /**
     * Update code
     */
    vis.setCode();

    /**
     * Create map
     */
    const cMap = vis.config.state.map;
    vis.map = new mapboxgl.Map({
      container: vis.id,
      style: vis.getStyle(),
      center: cMap.center,
      zoom: cMap.zoom,
      pitch: cMap.pitch,
      bearing: cMap.bearing,
    });

    vis.map.on('load', () => {
      if (vis.config.state.enabled) {
        vis.render();
      }
      //vis.map.setLight({ anchor: 'viewport' });
    });
    vis.map.on('moveend', vis.saveMapPos);
    /**
     * time
     */
    vis.t = 0;
  }
  saveMapPos() {
    const vis = this;
    const cMap = vis.config.state.map;
    const map = vis.map;
    cMap.bearing = map.getBearing();
    cMap.pitch = map.getPitch();
    cMap.zoom = map.getZoom();
    cMap.center = map.getCenter();
  }

  toggle() {
    const vis = this;
    const s = !vis.config.state.enabled;
    if (s) {
      vis.start();
    } else {
      vis.stop();
    }
  }
  start() {
    const vis = this;
    const s = vis.config.state;
    if (s.enabled) {
      return;
    }
    s.enabled = true;
    vis.render();
  }

  stop() {
    const s = this.config.state;
    s.enabled = false;
  }

  urlSave(e) {
    const vis = this;
    if (e instanceof Event) {
      e.preventDefault();
    }
    const state = vis.getState();
    const url = new URL(document.location);
    url.searchParams.set('state', JSON.stringify(state));
    history.replaceState(null, null, url);
    return url;
  }

  urlRestore(e) {
    const vis = this;
    if (e instanceof Event) {
      e.preventDefault();
    }
    const url = new URL(document.location);
    if (url.searchParams.has('state')) {
      try {
        const stateToRestore = JSON.parse(url.searchParams.get('state'));
        vis.setState(stateToRestore);
      } catch (err) {
        console.warn('Issue when restoring state', err);
      }
    }
  }

  getState() {
    return this.config.state;
  }

  setState(state) {
    const vis = this;
    const origState = vis.config.state;
    state = Object.assign({}, origState, state);
    for (let k in state) {
      state[k] = Object.assign({}, origState[k], state[k]);
    }
    vis.config.state = state;
  }

  getStyle() {
    const vis = this;
    return {
      version: 8,
      name: 'tixy3d',
      sources: {
        gj: { type: 'geojson', data: vis.featuresCollection() },
      },
      layers: [
        {
          id: 'back',
          type: 'background',
          paint: {
            'background-color': vis.config.state.map.backgroundColor,
          },
        },
        {
          id: 'gj',
          source: 'gj',
          type: 'fill-extrusion',
          minzoom: 0,
          paint: {
            'fill-extrusion-height-transition': {
              duration: 0,
              delay: 0,
            },
            'fill-extrusion-height': ['feature-state', 'h'],
            'fill-extrusion-color': ['feature-state', 'c'],
          },
        },
      ],
    };
  }

  updateBackGroundColor() {
    const color = vis.config.state.map.backgroundColor;
    vis.map.setPaintProperty('back', 'background-color', color);
  }
  updateLightColor() {
    const color = vis.config.state.map.lightColor;
    vis.map.setLight({ color: color });
  }
  updateLightPosition() {
    const r = vis.config.state.map.lightDistance;
    const a = vis.config.state.map.lightAzimutal;
    const p = vis.config.state.map.lightPolar;
    vis.map.setLight({ position: [r, a, p] });
  }

  //updateOpacity(alpha) {
  //const vis = this;
  //alpha = alpha || vis.config.state.grid.opacity;
  //vis.map.setPaintProperty('gj', 'fill-extrusion-opacity', alpha);
  /*}*/

  updateGrid() {
    const vis = this;
    vis.map.getSource('gj').setData(vis.featuresCollection());
  }

  featuresCollection() {
    const vis = this;
    const g = vis.config.state.grid;
    let id = 0;
    const gj = {
      type: 'FeatureCollection',
      features: [],
    };
    for (let lat = 0; lat < g.nY; lat++) {
      for (let lng = 0; lng < g.nX; lng++) {
        gj.features.push(
          vis.feature(id++, { lat: lat - g.nY / 2, lng: lng - g.nX / 2 }, {}),
        );
      }
    }
    return gj;
  }

  feature(id, coord, properties) {
    const vis = this;
    const g = vis.config.state.grid;
    return {
      type: 'Feature',
      id: id,
      properties: Object.assign({}, properties),
      geometry: {
        type: 'Polygon',
        coordinates: vis.buffer(
          coord.lng,
          coord.lat,
          g.radiusDeg,
          g.nVertices,
          g.orientationDeg,
        ),
      },
    };
  }

  toRad(deg) {
    return (deg * Math.PI * 2) / 360;
  }
  buffer(x, y, r, l, o) {
    const vis = this;
    let pX, pY, theta, first;
    const a = (2 * Math.PI) / l;
    const line = [];
    for (let i = 0; i < l; i++) {
      theta = a * i + vis.toRad(o);
      pX = r * Math.cos(theta) + x;
      pY = r * Math.sin(theta) + y;
      if (i === 0) {
        first = [pX, pY];
      }
      line.push([pX, pY]);
    }
    line.push(first);
    return [line];
  }

  getCode() {
    return this.config.state.main.code;
  }

  setCode(txt) {
    const vis = this;
    const c = vis.config;
    const o = c.state.main;

    try {
      if (txt) {
        o.code = txt;
      } else {
        o.code = o.code || c.code_default;
      }
      if (!o.code) {
        o.code = c.code_default;
      }
      o.code = o.code.replace(/\\/g, ';');
      vis._fun = new Function(
        't',
        'i',
        'x',
        'y',
        `
    let out = 0;
    try{
        with (Math) {
          return Number(${o.code});
        }
     }catch(e){};
    `,
      );
    } catch (e) {}
  }

  render() {
    const vis = this;
    const s = vis.config.state;
    vis.t++;
    let x,
      y,
      i = 0;
    let colA = s.grid.colorA;
    let colB = s.grid.colorB;
    let col = colA;
    for (x = 0; x < s.grid.nX; x++) {
      for (y = 0; y < s.grid.nY; y++) {
        let h =
          vis._fun((vis.t / 1000) * s.main.speedFactor, i, x, y) *
          s.grid.heightFactor;
        col = colA;
        if (h < 0) {
          h = -h;
          col = colB;
        }
        if (h > s.grid.heightMax) {
          h = s.grid.heightMax;
        }
        vis.map.setFeatureState({ source: 'gj', id: i }, { h: h, c: col });
        i++;
      }
    }
    if (s.enabled) {
      onNextFrame(vis.render);
    }
  }
}

export { Visualiser };
