# Tixy3d

Configurable 3d clone of [aemkei/tixy](https://github.com/aemkei/tixy).

![alt text](./static/tixy3d.gif "tixiac ui")


### Links

Original app: [tixy.land](https://tixy.land/).

### Dev 
`npm run start`

### Prod
`npm run build`
